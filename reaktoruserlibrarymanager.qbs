import qbs
import qbs.Probes

Project {
    Probes.ConanfileProbe {
        id: conan
        conanfilePath: project.sourceDirectory + "/conanfile.txt"
        generators: "qbs"
        additionalArguments: "-b=missing"
    }
    references: [conan.generatedFilesPath + "/conanbuildinfo.qbs"]

    CppApplication {

        Depends {
            name: "Qt.concurrent"
        }
        Depends {
            name: "Qt.quick"
        }
        Depends {
            name: "Qt.webengine"
        }
        Depends {
            name: "Qt.webchannel"
        }
        Depends {
            name: "Qt.sql"
        }
        Depends {
            name: "Qt.svg"
        }
        Depends {
            name: "libarchive"
        }

        cpp.cxxLanguageVersion: "c++17"
        cpp.staticLibraries: "acl"

        Group {
            name: "source"
            files: ["src/*"]
        }

        Group {
            name: "qarchive"
            files: ["external/QArchive/src/*", "external/QArchive/include/*"]
        }

        Group {
            name: "kdtoolbox"
            files: "external/KDToolBox/qt/model_view/KDTableToListProxyModel/src/*"
        }
        cpp.includePaths: ["external/KDToolBox/qt/model_view/KDTableToListProxyModel/src", "external/QArchive/include"]

        Group {
            name: "qml"
            fileTags: "qt.core.resource_data"
            files: ["src/qml/*", "src/js/*"]
        }

        Group {
            name: "images"
            fileTags: "qt.core.resource_data"
            files: ["images/*"]
        }
        Group {
            // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
            qbs.installDir: "bin"
        }
    }
}
