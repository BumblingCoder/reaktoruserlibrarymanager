#pragma once

#include <QNetworkAccessManager>
#include <QObject>
#include <QWebEngineCookieStore>

#include "applicationpreferences.h"
#include "buttonproperties.h"
class PluginDownloader : public QObject {
  Q_OBJECT
public:
  PluginDownloader(QObject* parent,
                   ButtonProperties* lastClickedButton,
                   ApplicationPreferences* preferences,
                   QWebEngineCookieStore* cookies);

  Q_PROPERTY(int progress READ progress NOTIFY progressChanged)
  Q_PROPERTY(bool downloading READ downloading NOTIFY downloadingChanged)

  int progress() const;
  bool downloading() const;

  QString latestVersion(int id) const;

public slots:
  void downloadRequested(QString downloadURL, QString currentPage);
  void updateAllPlugins();

signals:
  void progressChanged(int progress);
  void downloadingChanged(bool downloading);

private:
  QString urlForPlugin(int pluginID) const;
  QString getPageText(int pluginID) const;

private:
  ButtonProperties* m_lastClickedButton = nullptr;
  ApplicationPreferences* m_preferences = nullptr;
  QNetworkAccessManager* m_network = nullptr;
  int m_progress = 0;
  bool m_downloading = false;
  std::map<int, QString> m_pluginVersions;
};
