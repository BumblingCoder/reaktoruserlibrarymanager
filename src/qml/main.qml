import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.0
import QtWebEngine 1.10

Window {
    visibility: "Maximized"
    Rectangle {
        id: topBar
        height: 40
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        color: "#2a2c2e"
    }

    Frame {
        id: leftFrame
        anchors.top: topBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        background: Rectangle {
            color: "#f9f9f9"
            border.color: "#e4e4e4"
        }

        ButtonGroup {
            buttons: column.children
        }

        ColumnLayout {
            id: column
            Button {
                id: addButton
                icon.name: "list-add"
                icon.width: 64
                icon.height: 64
                checked: true
                checkable: true
            }
            Button {
                id: updateButton
                icon.name: "update-none"
                icon.width: 64
                icon.height: 64
                checkable: true
            }
            Button {
                id: settingsButton
                icon.name: "settings-configure"
                icon.width: 64
                icon.height: 64
                checkable: true
            }
        }
    }

    Item {
        anchors.top: topBar.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: leftFrame.right
        WebAddPage {
            anchors.fill: parent
            visible: addButton.checked
        }
        UpdatePage {
            anchors.fill: parent
            visible: updateButton.checked
        }

        SettingsPage {
            anchors.fill: parent
            visible: settingsButton.checked
        }
    }
    WebAddPage {
        // Force the user to log in if they aren't by default.
        url: "https://www.native-instruments.com/en/my-account"
        visible: url.toString().includes("auth.native-instruments.com/login")
        anchors.fill: parent
        z: 100
    }
    Component.onCompleted: {
        webprofile.onDownloadRequested.connect(function handleRequest(request) {
            pluginDownloader.downloadRequested(request.url,
                                               WebAddLastURL.lastURL)
        })
    }
}
