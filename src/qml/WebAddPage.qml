import QtWebEngine 1.10
import QtQuick.Controls 2.0
import QtQuick 2.0
import QtQuick.Layouts 1.0

Frame {
    id: webFrame
    padding: 0
    default property alias url: addWebView.url
    ProgressBar {
        id: pageProgress
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        from: 0
        to: 100
        value: addWebView.loadProgress
        height: 5
    }
    WebEngineView {
        visible: !loading
        id: addWebView
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        url: "https://www.native-instruments.com/en/reaktor-community/reaktor-user-library"
        profile: webprofile
        webChannel: myChannel
        webChannelWorld: 1
        userScripts: [
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: "qrc:///qtwebchannel/qwebchannel.js"
                worldId: "ApplicationWorld"
            },
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentReady
                sourceUrl: "buttonOverrideUserScript.js"
                worldId: "ApplicationWorld"
            }
        ]
        onUrlChanged: {
            WebAddLastURL.lastURL = url
        }
    }
    Frame {
        anchors.bottom: parent.bottom
        background: Rectangle {
            color: "white"
            border.color: "black"
        }
        anchors.horizontalCenter: parent.horizontalCenter
        visible: pluginDownloader.downloading
        ColumnLayout {
            Label {
                text: "Downloading " + lastClickedButton.name
            }
            ProgressBar {
                from: 0
                to: 100
                value: pluginDownloader.progress
            }
        }
    }
}
