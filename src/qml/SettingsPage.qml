import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    ColumnLayout {
        RowLayout {
            Label {
                text: "Reaktor User Library Path"
            }
            TextField {
                text: preferences.reaktorLibraryPath
                onTextEdited: {
                    preferences.setReaktorLibraryPath(text)
                }
            }
        }
    }
}
