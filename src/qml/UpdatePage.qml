import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    anchors.fill: parent
    property int pluginToLookAt: 0
    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Layout.fillWidth: true
            height: childrenRect.height
            id: header
            Layout.margins: 10
            Label {
                text: "Installed Items"
                font.bold: true
                font.pointSize: 20
                Layout.alignment: Qt.AlignVCenter
            }
            Item {
                Layout.fillWidth: true
            }
            Button {
                text: "Update All"
                icon.name: "download"
                icon.color: "white"
                palette.buttonText: "white"
                Layout.alignment: Qt.AlignVCenter
                background: Rectangle {
                    color: "#3e8cbb"
                    radius: 3
                }
                onClicked: pluginDownloader.updateAllPlugins()
            }
        }
        ListView {
            model: installedItemsModel
            Layout.fillHeight: true
            Layout.fillWidth: true
            z: -1
            delegate: Rectangle {
                color: index % 2 == 0 ? "#f9f9f9" : "#ffffff"
                anchors.left: parent.left
                anchors.right: parent.right
                width: parent.parent.height
                height: 40
                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        pluginToLookAt = PluginID
                    }
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    x: 10
                    text: Name
                }

                Label {
                    x: parent.width * 0.3
                    anchors.verticalCenter: parent.verticalCenter
                    text: Version
                    color: "gray"
                }
                Label {
                    x: parent.width * 0.6
                    anchors.verticalCenter: parent.verticalCenter
                    text: Size
                    color: "gray"
                }
                Button {
                    icon.name: HasUpdate ? "download" : "checkmark"
                    icon.color: "gray"
                    text: HasUpdate ? "Update" : "Installed"
                    palette.buttonText: "gray"
                    anchors.verticalCenter: parent.verticalCenter
                    x: parent.width * 0.8
                    background: Item {}
                }
                Button {
                    icon.name: "open-menu-symbolic"
                    visible: mouseArea.containsMouse
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    background: Item {}

                    onClicked: menu.popup()
                    Menu {
                        id: menu
                        MenuItem {
                            text: "Show Web"
                            onClicked: {
                                pluginToLookAt = PluginID
                            }
                        }
                        MenuItem {
                            text: "Remove (unimplemented)"
                        }
                    }
                }
            }
        }
    }
    Rectangle {
        visible: webFrame.visible
        z: 1
        color: "white"
        opacity: 0.4
        anchors.fill: parent
        MouseArea {
            anchors.fill: parent
            onClicked: {
                pluginToLookAt = 0
            }
        }
    }

    Frame {
        id: webFrame
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: visible ? parent.width / 3 : 0
        visible: pluginToLookAt != 0
        padding: 1
        z: 2
        WebAddPage {
            anchors.fill: parent
            url: "https://www.native-instruments.com/en/reaktor-community/reaktor-user-library/entry/show/" + pluginToLookAt
        }
    }
}
