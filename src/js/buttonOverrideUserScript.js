// ==UserScript==
// @include *
// ==/UserScript==
var webChannel = new QWebChannel(qt.webChannelTransport, function (channel) {})

var buttons = document.getElementsByClassName("btn-group")
for (var i = 0; i < buttons.length; i++) {
    let button = buttons[i]
    button.getElementsByClassName("btn-download")[0].addEventListener(
                "click", function () {
                    var buttonProperties = webChannel.objects.buttonProperties

                    buttonProperties.setScript(
                                String(button.parentNode.getElementsByTagName(
                                           "script")[0].text))
                    buttonProperties.setDataId(
                                String(
                                    button.getElementsByTagName(
                                        "a")[0].attributes["data-id"].nodeValue))
                    buttonProperties.setName(
                                String(
                                    button.getElementsByTagName(
                                        "a")[0].attributes["data-name"].nodeValue))
                    buttonProperties.setVersion(
                                String(
                                    button.getElementsByTagName(
                                        "a")[0].attributes["data-version"].nodeValue))

                    var titleBlock = button.parentNode.parentNode.parentNode.parentNode.getElementsByClassName(
                                "description-title")[0]

                    if (titleBlock) {
                        buttonProperties.setUrl(
                                    String(
                                        titleBlock.getElementsByTagName(
                                            "a")[0].attributes["href"].nodeValue))
                    } else {
                        buttonProperties.setUrl("")
                    }
                })
}

document.getElementById("header").remove()
document.getElementById("footer").remove()
document.getElementsByClassName("content-header-container")[0].remove()
