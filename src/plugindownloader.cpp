#include "plugindownloader.h"

#include <QDir>
#include <QEventLoop>
#include <QFile>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QtConcurrentRun>
#include <qarchivediskextractor.hpp>

#include "database.h"

PluginDownloader::PluginDownloader(QObject* parent,
                                   ButtonProperties* lastClickedButton,
                                   ApplicationPreferences* preferences,
                                   QWebEngineCookieStore* cookies)
    : QObject(parent)
    , m_lastClickedButton(lastClickedButton)
    , m_preferences(preferences)
    , m_network(new QNetworkAccessManager(this)) {
  connect(cookies,
          &QWebEngineCookieStore::cookieAdded,
          m_network->cookieJar(),
          &QNetworkCookieJar::insertCookie);
  cookies->loadAllCookies();
  QtConcurrent::run([this]() {
    for(auto pluginID : Database::allPluginIDs()) {
      auto pageText = getPageText(pluginID);
      QRegularExpression re(R"(data-version="([^"]+))");
      auto match = re.match(pageText);
      m_pluginVersions[pluginID] = match.captured(1);
    }
  });
}

int PluginDownloader::progress() const { return m_progress; }

bool PluginDownloader::downloading() const { return m_downloading; }

QString PluginDownloader::latestVersion(int id) const {
  if(m_pluginVersions.count(id)) {
    return m_pluginVersions.at(id);
  } else {
    return "";
  }
}

void PluginDownloader::downloadRequested(QString downloadURL, QString currentPage) {
  // TODO: Save info about what was downloaded and to where.
  m_progress = 0;
  m_downloading = true;
  progressChanged(m_progress);
  downloadingChanged(m_downloading);
  auto reply = m_network->get(QNetworkRequest(QUrl(downloadURL)));
  reply->setReadBufferSize(0);
  connect(reply, &QNetworkReply::downloadProgress, this, [&](auto recieved, auto total) {
    m_progress = 100 * recieved / total;
    progressChanged(m_progress);
  });
  connect(reply, &QNetworkReply::finished, this, [=]() {
    m_downloading = false;
    downloadingChanged(m_downloading);
    QDir writeDir(m_preferences->reaktorLibraryPath());
    writeDir.mkpath(m_lastClickedButton->name());
    writeDir.cd(m_lastClickedButton->name());
    auto shortFilename = downloadURL.split('/').last();
    auto filename = writeDir.absoluteFilePath(shortFilename);
    QFile f(filename);
    f.open(QFile::WriteOnly);
    f.write(reply->readAll());
    f.close();
    if(filename.endsWith("zip") || filename.endsWith("rar")) {
      auto outputDirectory = writeDir.absolutePath();
      QArchive::DiskExtractor e(filename, outputDirectory);
      QEventLoop el;
      connect(&e, &QArchive::DiskExtractor::finished, &el, &QEventLoop::quit);
      e.start();
      el.exec();
    }

    QStringList absoluteFiles;
    for(const auto& fileInfo : writeDir.entryInfoList(QDir::Files)) {
      absoluteFiles.push_back(fileInfo.absoluteFilePath());
    }

    QRegularExpression re(R"(\/show\/(\d+))");
    auto match =
        re.match(m_lastClickedButton->url().isEmpty() ? currentPage : m_lastClickedButton->url());
    Database::pluginDownloaded(m_lastClickedButton->name(),
                               downloadURL,
                               absoluteFiles,
                               match.captured(1).toInt(),
                               m_lastClickedButton->version());
  });
}

void PluginDownloader::updateAllPlugins() {
  for(auto id : Database::allPluginIDs()) {
    if(Database::getInstalledVersion(id) == m_pluginVersions[id]) { continue; }

    // The user library uses an internal ID to ge the file to download. The way this works is that
    // you request a predictable URL with a data ID and then get redirected to the actual file URL.
    // There doesn't seem to be ane easy way to go from the user lbrary  ID to the data ID without
    // downloading the web page, so that is what we are doing here.

    QRegularExpression regexForId(R"(data-id="(\d+))");
    const auto pageText = getPageText(id);
    auto idmatch = regexForId.match(pageText);
    auto dataID = idmatch.captured(1);

    downloadRequested(
        QString("https://www.native-instruments.com/en/reaktor-community/reaktor-user-library/"
                "entry/download/?tx_niuserlib_niuserlib[file]=%1")
            .arg(dataID),
        urlForPlugin(id));
  }
}

QString PluginDownloader::urlForPlugin(int pluginID) const {
  return QString(
             "https://www.native-instruments.com/en/reaktor-community/"
             "reaktor-user-library/entry/show/%1/")
      .arg(pluginID);
}

QString PluginDownloader::getPageText(int pluginID) const {
  const QString pageURL = urlForPlugin(pluginID);
  QEventLoop el;
  auto pageReply = m_network->get(QNetworkRequest(pageURL));
  pageReply->setReadBufferSize(0);
  connect(pageReply, &QNetworkReply::finished, &el, &QEventLoop::quit);
  el.exec();
  return pageReply->readAll();
}
