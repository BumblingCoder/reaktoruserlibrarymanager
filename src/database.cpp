#include "database.h"

#include <QDir>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QVariant>

#include <iostream>
Database::Database() {
  m_db = QSqlDatabase::addDatabase("QSQLITE");
  m_db.setDatabaseName(QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
                           .absoluteFilePath("ReaktorUserLibraryManagerDB.sqlite"));
  if(!m_db.open()) { throw std::runtime_error("Couldn't open the database!"); }

  QSqlQuery q;
  q.exec(R"(CREATE TABLE IF NOT EXISTS "LibraryContent" (
         "Name"	TEXT NOT NULL,
         "DownloadURL"	TEXT NOT NULL,
         "Files"	TEXT NOT NULL,
         "PluginID"	INTEGER NOT NULL UNIQUE,
         "Version"	TEXT NOT NULL,
         PRIMARY KEY("PluginID")
         );)");
}

void Database::pluginDownloaded(QString name,
                                QString downloadURL,
                                QStringList files,
                                int pluginID,
                                QString version) {
  QSqlQuery q;
  q.prepare("REPLACE INTO LibraryContent VALUES(?, ?, ?, ?, ?);");
  q.addBindValue(name);
  q.addBindValue(downloadURL);
  q.addBindValue(files.join(";"));
  q.addBindValue(pluginID);
  q.addBindValue(version);
  q.exec();
}

std::vector<int> Database::allPluginIDs() {
  QSqlQuery q;
  q.exec("SELECT PluginID FROM LibraryContent;");
  std::vector<int> ids;
  while(q.next()) { ids.push_back(q.value(0).toInt()); }
  return ids;
}

QString Database::getInstalledVersion(int pluginID) {
  QSqlQuery q;
  q.prepare("SELECT Version FROM LibraryContent WHERE PluginID=?");
  q.addBindValue(pluginID);
  q.exec();
  return q.value(0).toString();
}
