#pragma once

#include <QObject>

class ButtonProperties : public QObject {
  Q_OBJECT

public:
  explicit ButtonProperties(QObject* parent = nullptr);
  Q_PROPERTY(QString dataId READ dataId WRITE setDataId NOTIFY dataIdChanged)
  Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)
  Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
  Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(QString script READ script WRITE setScript NOTIFY scriptChanged)

  QString dataId() const { return m_dataId; }
  QString version() const { return m_version; }
  QString url() const { return m_url; }
  QString name() const { return m_name; }
  QString script() const { return m_script; }

public slots:
  void setDataId(QString dataId) {
    if(m_dataId == dataId) return;

    m_dataId = dataId;
    emit dataIdChanged(m_dataId);
  }

  void setVersion(QString version) {
    if(m_version == version) return;

    m_version = version;
    emit versionChanged(m_version);
  }

  void setUrl(QString url) {
    if(m_url == url) return;

    m_url = url;
    emit urlChanged(m_url);
  }

  void setName(QString name) {
    if(m_name == name) return;

    m_name = name;
    emit nameChanged(m_name);
  }

  void setScript(QString script) {
    if(m_script == script) return;

    m_script = script;
    emit scriptChanged(m_script);
  }

signals:
  void dataIdChanged(QString dataId);
  void versionChanged(QString version);
  void urlChanged(QString url);
  void nameChanged(QString name);
  void scriptChanged(QString script);

private:
  QString m_dataId;
  QString m_version;
  QString m_url;
  QString m_name;
  QString m_script;
};
