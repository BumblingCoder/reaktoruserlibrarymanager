#pragma once

#include <KDTableToListProxyModel.h>
#include <QAbstractItemModel>
#include <QIdentityProxyModel>
#include <QSqlTableModel>

#include "plugindownloader.h"

class InstalledItemsModel : public KDTableToListProxyModel {
public:
  InstalledItemsModel(QObject* parent, PluginDownloader* downloader);
  ~InstalledItemsModel() = default;

private:
  class InstalledItemsAddonModel : public QAbstractProxyModel {
  public:
    InstalledItemsAddonModel(QObject* parent, PluginDownloader* downloader);
    ~InstalledItemsAddonModel() = default;

    QModelIndex mapToSource(const QModelIndex& proxyIndex) const override;
    QModelIndex mapFromSource(const QModelIndex& sourceIndex) const override;
    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent) const override;
    int columnCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role) const override;

    QSqlTableModel* sqlModel();

  private:
    QSqlTableModel* m_model;
    PluginDownloader* m_pluginDownloader;
  };
};
