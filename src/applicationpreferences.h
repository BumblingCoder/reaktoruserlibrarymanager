#pragma once

#include <QObject>
#include <QSettings>

class ApplicationPreferences : public QObject {
  Q_OBJECT

public:
  explicit ApplicationPreferences(QObject* parent = nullptr);

  Q_PROPERTY(QString reaktorLibraryPath READ reaktorLibraryPath WRITE setReaktorLibraryPath NOTIFY
                 reaktorLibraryPathChanged)
  QString reaktorLibraryPath() const;

public slots:
  void setReaktorLibraryPath(QString reaktorLibraryPath);

signals:
  void reaktorLibraryPathChanged(QString reaktorLibraryPath);

private:
  QString m_reaktorLibraryPath;
  QSettings m_settings;
};
