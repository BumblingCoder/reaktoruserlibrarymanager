#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlWebChannel>
#include <QQuickWebEngineProfile>
#include <QSqlTableModel>

#include "applicationpreferences.h"
#include "buttonproperties.h"
#include "database.h"
#include "installeditemsmodel.h"
#include "plugindownloader.h"

int main(int argc, char* argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);
  app.setWindowIcon(QIcon(":/NI_Reaktor_Logo.svg"));
  app.setDesktopFileName("reaktoruserlibrarymanager.desktop");

  QCoreApplication::setOrganizationName("bumblingcoder");
  QCoreApplication::setApplicationName("Reaktor User Library Manager");

  Database db;
  QQuickWebEngineProfile profile;
  profile.setStorageName("ReaktorUserLibraryManager");
  profile.setPersistentCookiesPolicy(QQuickWebEngineProfile::ForcePersistentCookies);
  profile.setOffTheRecord(false);

  auto* properties = new ButtonProperties(&app);
  auto* preferences = new ApplicationPreferences(&app);
  auto* downloader = new PluginDownloader(&app, properties, preferences, profile.cookieStore());

  // We use a web channel to update the properties in ButtonProperties from a user script
  QQmlWebChannel channel;
  channel.registerObject(QStringLiteral("buttonProperties"), properties);

  auto model = new InstalledItemsModel(&app, downloader);

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("myChannel", QVariant::fromValue(&channel));
  engine.rootContext()->setContextProperty("pluginDownloader", downloader);
  engine.rootContext()->setContextProperty("preferences", preferences);
  engine.rootContext()->setContextProperty("lastClickedButton", properties);
  engine.rootContext()->setContextProperty("installedItemsModel", model);
  engine.rootContext()->setContextProperty("webprofile", &profile);
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine,
      &QQmlApplicationEngine::objectCreated,
      &app,
      [url](QObject* obj, const QUrl& objUrl) {
        if(!obj && url == objUrl) QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
