#include "installeditemsmodel.h"

#include <KDTableToListProxyModel.h>
#include <QFileInfo>
#include <QSqlRecord>
#include <QSqlTableModel>

InstalledItemsModel::InstalledItemsModel(QObject* parent, PluginDownloader* downloader)
    : KDTableToListProxyModel(parent) {
  auto source = new InstalledItemsAddonModel(this, downloader);
  setSourceModel(source);
  auto record = source->sqlModel()->record();
  for(int i = 0; i < record.count(); ++i) {
    setRoleMapping(i, Qt::UserRole + i, record.fieldName(i).toLocal8Bit());
  }
  setRoleMapping(record.count(), Qt::UserRole + record.count(), "Size");
  setRoleMapping(record.count() + 1, Qt::UserRole + record.count() + 1, "HasUpdate");
}

InstalledItemsModel::InstalledItemsAddonModel::InstalledItemsAddonModel(
    QObject* parent,
    PluginDownloader* downloader)
    : QAbstractProxyModel(parent), m_pluginDownloader(downloader) {
  m_model = new QSqlTableModel(this);
  m_model->setTable("LibraryContent");
  m_model->select();
  setSourceModel(m_model);
}

QModelIndex
InstalledItemsModel::InstalledItemsAddonModel::mapToSource(const QModelIndex& proxyIndex) const {
  if(proxyIndex.column() >= sourceModel()->columnCount()) { return {}; }
  return index(proxyIndex.row(), proxyIndex.column());
}

QModelIndex
InstalledItemsModel::InstalledItemsAddonModel::mapFromSource(const QModelIndex& sourceIndex) const {
  return index(sourceIndex.row(), sourceIndex.column());
}

QModelIndex
InstalledItemsModel::InstalledItemsAddonModel::index(int row,
                                                     int column,
                                                     const QModelIndex& /*parent*/) const {
  return createIndex(row, column);
}

QModelIndex
InstalledItemsModel::InstalledItemsAddonModel::parent(const QModelIndex& /*index*/) const {
  return {};
}

int InstalledItemsModel::InstalledItemsAddonModel::rowCount(const QModelIndex& parent) const {
  return sourceModel()->rowCount(parent);
}

int InstalledItemsModel::InstalledItemsAddonModel::columnCount(const QModelIndex& parent) const {
  const auto sourceColumns = sourceModel()->columnCount(parent);
  if(sourceColumns != 0) { return sourceColumns + 2; }
  return 0;
}

QVariant InstalledItemsModel::InstalledItemsAddonModel::data(const QModelIndex& index,
                                                             int role) const {
  const auto sourceColumns = sourceModel()->columnCount(index.parent());
  if(index.column() == sourceColumns) {
    // TODO: support multiple files, for when we unzip archives.
    const auto files = sourceModel()->data(index.siblingAtColumn(2)).toString().split(";");
    qint64 totalSize = 0;
    for(const auto& file : files) {
      QFileInfo info(file);
      totalSize += info.size();
    }
    return totalSize;
  } else if(index.column() == sourceColumns + 1) {
    const auto version = sourceModel()->data(index.siblingAtColumn(4)).toString();
    const int pluginID = index.siblingAtColumn(4).data().toInt();
    const QString latestVersion = m_pluginDownloader->latestVersion(pluginID);
    return latestVersion.size() > 0 && version != latestVersion;
  }
  return sourceModel()->data(index, role);
}

QSqlTableModel* InstalledItemsModel::InstalledItemsAddonModel::sqlModel() { return m_model; }
