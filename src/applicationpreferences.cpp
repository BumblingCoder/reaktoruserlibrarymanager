#include "applicationpreferences.h"

#include <QDir>
#include <QSysInfo>

static const char* libraryPathKey = "ReaktorLibraryPath";

ApplicationPreferences::ApplicationPreferences(QObject* parent) : QObject(parent) {
  // TODO: this should probably get a better default location.
  const auto defaultPath =
      QSysInfo::kernelType() == "winnt"
          ? QDir::homePath() + "\\My Documents\\Native Instruments\\Reaktor 6\\Ensembles"
          : QDir::homePath() + "/Documents/Native Instruments/Reaktor 6/Ensembles";
  m_reaktorLibraryPath = m_settings.value(libraryPathKey, defaultPath).toString();
}

QString ApplicationPreferences::reaktorLibraryPath() const { return m_reaktorLibraryPath; }

void ApplicationPreferences::setReaktorLibraryPath(QString reaktorLibraryPath) {
  if(m_reaktorLibraryPath == reaktorLibraryPath) return;
  m_reaktorLibraryPath = reaktorLibraryPath;
  emit reaktorLibraryPathChanged(m_reaktorLibraryPath);
  m_settings.setValue(libraryPathKey, m_reaktorLibraryPath);
}
