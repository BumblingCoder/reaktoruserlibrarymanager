#pragma once

#include <QSqlDatabase>
#include <QVector>

class Database {
public:
  Database();

  static void pluginDownloaded(QString name,
                               QString downloadURL,
                               QStringList files,
                               int pluginID,
                               QString version);
  static std::vector<int> allPluginIDs();
  static QString getInstalledVersion(int pluginID);

private:
  QSqlDatabase m_db;
};
